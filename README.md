Just clone this repository into your data volume of SteamCMD container and configure it by environment variables.

# Environment variables
```
# KEY=DEFAULT VALUE
WORLD_SIZE=4000
WORLD_SEED=1234
WORLD_MAX_PLAYERS=10
SERVER_NAME="Dockerized Rust"
SERVER_DESCRIPTION="My dockerized Rust server."
WEBSITE_URL="http://example.org"
JPG_SERVER_HEADER="http://example.org/example.jpg"
SERVER_IDENTITY=server1
RCON_PASSWORD=MY_DOCKERIZED_RUST_RCON
RCON_WEB=1
CUSTOM_OPTIONS=
```
