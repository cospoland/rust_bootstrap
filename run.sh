#!/bin/bash
steamcmd_dir="/data/steamcmd"
install_dir="/data/pkg"

function fail()
{
        echo Error: "$@" >&2
        exit 1
}

function check_for_file()
{
    if [ ! -e "$1" ]; then
            fail "Missing file: $1"
    fi
}

cd "$steamcmd_dir" || fail "Missing $steamcmd_dir directory!"

check_for_file "steamcmd.sh"

./steamcmd.sh +force_install_dir "$install_dir" +login anonymous +app_update 258550 validate +quit

WORLD_SIZE=${WORLD_SIZE:-4000}
WORLD_SEED=${WORLD_SEED:-1234}
WORLD_MAX_PLAYERS=${WORLD_MAX_PLAYERS:-10}
SERVER_NAME=${SERVER_NAME:-"Dockerized Rust"}
SERVER_DESCRIPTION=${SERVER_DESCRIPTION:-"My dockerized Rust server."}
WEBSITE_URL=${WEBSITE_URL:-"http://example.org"}
JPG_SERVER_HEADER=${JPG_SERVER_HEADER:-"http://example.org/example.jpg"}
SERVER_IDENTITY=${SERVER_IDENTITY:-server1}
RCON_PASSWORD=${RCON_PASSWORD:-MY_DOCKERIZED_RUST_RCON}
RCON_WEB=${RCON_WEB:-1}

cd $install_dir
LD_LIBRARY_PATH=$install_dir:$install_dir/RustDedicated_Data/Plugins/x86_64:{$LD_LIBRARY_PATH}
export LD_LIBRARY_PATH
exec ./RustDedicated -logfile /dev/stdout -batchmode +server.port 28015 -nographics +server.level "Procedural Map" +server.seed "$WORLD_SEED" +server.worldsize "$WORLD_SIZE" +server.maxplayers "$WORLD_MAX_PLAYERS" \
                  +server.hostname "$SERVER_NAME" +server.description "$SERVER_DESCRIPTION" +server.url "$WEBSITE_URL" +server.headerimage "$JPG_SERVER_HEADER" \
				  +server.identity "$SERVER_IDENTITY" +rcon.port 28016 $CUSTOM_OPTIONS +rcon.password "$RCON_PASSWORD" +rcon.web $RCON_WEB